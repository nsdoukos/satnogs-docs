SatNOGS Station Architecture
============================

Installed components
--------------------

.. vale off
.. # Mermaid definition for the following image:
.. graph TD
..     A(satnogs-client) -->|invokes flowgraph from| B(satnogs-flowgraphs)
..     B -->|depends on| C(gr-satnogs)
..     B -->|depends on| D(gr-soapy)
..     E(satnogs-config) -->|runs playbook & roles from| F(satnogs-ansible)

.. figure:: station-components-overview.png
.. vale on

   SatNOGS Station Components Overview

* `satnogs-flowgraphs <https://gitlab.com/librespacefoundation/satnogs/satnogs-flowgraphs>`_:
  Provide the generic & satellites specific GNU Radio flowgraphs.

* `gr-soapy <https://gitlab.com/librespacefoundation/gr-soapy>`_:
  A GNU Radio wrapper for the SoapySDR library.

* `gr-satnogs <https://gitlab.com/librespacefoundation/satnogs/gr-satnogs>`_:
  GNU Radio Out-Of-Tree Module with blocks required by satnogs-flowgraphs,
  for example a waterfall sink.

* `satnogs-ansible <https://gitlab.com/librespacefoundation/satnogs/satnogs-ansible>`_:
  The Ansible playbook & roles for setting up and configuring a SatNOGS Station.

* `satnogs-config <https://gitlab.com/librespacefoundation/satnogs/satnogs-config>`_:
  SatNOGS client system configuration utility. Invokes roles from SatNOGS Ansible
  for applying the actual setup & configuration.

* `satnogs-client <https://gitlab.com/librespacefoundation/satnogs/satnogs-client>`_:
  Python Daemon which fetches jobs from SatNOGS network, controls rotators (via Hamlib),
  invokes flowgraphs from satnogs-flowgraphs for reception and finally uploads the observation
  results to satnogs-network. Optionally also uploads "SatNOGS artifacts" to satnogs-db.


Related components
------------------

* `satnogs-pi-gen <https://gitlab.com/librespacefoundation/satnogs/satnogs-pi-gen>`_:
  Tool used to create SatNOGS Raspbian images
