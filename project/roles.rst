#####
Roles
#####

The SatNOGS project embodies a non-hierarchical approach to project management
and development, aligning with self-management principles where volunteers and
contributors constantly assume and drop roles and responsibilities based on
their interests, skills, and the needs of the project.

Contributors to the SatNOGS project choose their own roles not by assignment,
but by aligning their skills, experience, and interests with the tasks that
need to be accomplished.

These roles may be limited by time or specific tasks and cease to exist when
they are no longer necessary.

*************
Current roles
*************

The table below lists the currently active roles within the SatNOGS project,
with each contributor individually maintaining their respective entries.

.. csv-table:: SatNOGS Project Roles
   :file: roles.csv
   :header-rows: 1

